package com.devcamp.rest_api;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class apiController {
    static Staff staff1 = new Staff(1, "Trần văn Ninh", 23);
    static Staff staff2 = new Staff(2, "Tôn Nữ Linh Giang", 28);
    static Staff staff3 = new Staff(3, "Lê Nguyễn Đức Thạnh", 29);
    static Staff staff4 = new Staff(4, "Nguyễn Hoàng Hải", 34);
    static Staff staff5 = new Staff(5, "tokuda", 23);
    static Staff staff6 = new Staff(6, "sakura", 35);
    static Staff staff7 = new Staff(7, "sdfsdfsdfsd", 12);
    static Staff staff8 = new Staff(8, "werwgdfgderwe", 20);
    static Staff staff9 = new Staff(9, "gdfgdfgdfgd", 16);
    static Department department1 = new Department(1, "nát", "mỹ tho", staffs1());
    static Department department2 = new Department(2, "bét", "mỹ tho", staffs2());
    static Department department3 = new Department(3, "quá", "mỹ tho", staffs3());

    public static ArrayList<Staff> staffs1() {
        ArrayList<Staff> staffs = new ArrayList<>();
        staffs.add(staff1);
        staffs.add(staff2);
        staffs.add(staff3);
        return staffs;
    }

    public static ArrayList<Staff> staffs2() {
        ArrayList<Staff> staffs = new ArrayList<>();
        staffs.add(staff4);
        staffs.add(staff5);
        staffs.add(staff6);
        return staffs;
    }

    public static ArrayList<Staff> staffs3() {
        ArrayList<Staff> staffs = new ArrayList<>();
        staffs.add(staff7);
        staffs.add(staff8);
        staffs.add(staff9);
        return staffs;
    }

    // api trả về danh sách tất cả các phòng
    @GetMapping("/all_department")
    public static ArrayList<Department> Department() {
        ArrayList<Department> Department = new ArrayList<>();
        Department.add(department1);
        Department.add(department2);
        Department.add(department3);
        return Department;
    }

    // api trả về danh sách tất cả nhân viên
    @GetMapping("/all_staff")
    public static ArrayList<Staff> staff() {
        ArrayList<Staff> staff = new ArrayList<>();
        staff.add(staff1);
        staff.add(staff2);
        staff.add(staff3);
        staff.add(staff4);
        staff.add(staff5);
        staff.add(staff6);
        staff.add(staff7);
        staff.add(staff8);
        staff.add(staff9);
        return staff;
    }

    // api trả về department id tương ứng
    @GetMapping("/department_id")
    public Department department_id(@RequestParam(value = "id", defaultValue = "1") int id) {
        ArrayList<Department> Department = Department();
        Department DepartmentFound = new Department();
        for (Department department : Department) {
            if (department.getId() == id) {
                DepartmentFound = department;
            }
        }
        return DepartmentFound;
    }

    // api trả về nhân viên có số tuổi đã truyền vào
    @GetMapping("/staff_age")
    public ArrayList<Staff> staff_age(@RequestParam(value = "age", defaultValue = "1") int age) {
        ArrayList<Staff> staff = staff();
        ArrayList<Staff> staffFound = new ArrayList<>();
        for (Staff staff10 : staff) {
            if (staff10.getAge() > age) {
                staffFound.add(staff10);
            }
        }
        return staffFound;
    }

    // api trả về về danh sách phòng ban có độ tuổi trung bình lớn hơn averageAge
    @GetMapping(value = "/averageAge")
    public ArrayList<Department> averageAge(@RequestParam(value = "averageAge", defaultValue = "1") float age) {
        ArrayList<Department> Department = Department();
        ArrayList<Department> DepartmentFound = new ArrayList<>();
        for (Department department : Department) {
            if (department.getAverageAge() > age) {
                DepartmentFound.add(department);
            }
        }
        return DepartmentFound;

    }

}
